package com.clubjumboprivileges.Model_class;

import java.util.List;

public class NotificationModel {

    private String status;
    private String message;
    private List<resulst> resulst = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NotificationModel.resulst> getResulst() {
        return resulst;
    }

    public void setResulst(List<NotificationModel.resulst> resulst) {
        this.resulst = resulst;
    }

    public class resulst {

        private String id;
        private String title;
        private String description;
        private String created_date;
        private String url_link;

        public String getUrl_link() {
            return url_link;
        }

        public void setUrl_link(String url_link) {
            this.url_link = url_link;
        }



        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }


    }

}
