package com.clubjumboprivileges.database;

public class Databasevalues {

	public static final String MYDATABASE_NAME="Loyality_Data";
	public static final int MYDATABASE_VERSION= 1;	
	public static final String TABLE_Name ="Scanning_data";
	public static final String USER_ID="User_id";
	public static final String USER_NAME="user_name";		
	public static final String BAR_CODE_DATA="barcodedata";
	public static final String DATEANDTIME="dateandtime";
	public static final String FORMAT="format";
	
	
	final static String SCRIPT_CREATE_TABLE_BAR_CODE = "create table "
			+ "" + TABLE_Name + " ("+ USER_ID + " INTEGER PRIMARY KEY   AUTOINCREMENT, " + USER_NAME + " TEXT, " 
								+ BAR_CODE_DATA + " TEXT, " + DATEANDTIME + " TEXT, " + FORMAT + " TEXT )";		
	
}
