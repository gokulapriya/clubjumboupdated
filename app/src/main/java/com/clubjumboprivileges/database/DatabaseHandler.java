package com.clubjumboprivileges.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler {


	private Context c;
	private SQLiteDatabase sqLiteDatabase;
	private SQLiteHelper sqLiteHelper;

	public DatabaseHandler(Context c) {
		this.c = c;
	}

	public DatabaseHandler opendb() {

		sqLiteHelper = new SQLiteHelper(c, Databasevalues.MYDATABASE_NAME, null, 0);
		sqLiteDatabase = sqLiteHelper.getWritableDatabase();
		return this;
	}
	public DatabaseHandler writeopendb() {
		sqLiteHelper = new SQLiteHelper(c, Databasevalues.MYDATABASE_NAME, null, Databasevalues.MYDATABASE_VERSION);
		sqLiteDatabase = sqLiteHelper.getWritableDatabase();
		return this;
	}
	public void closeconnection() {
		sqLiteHelper.close();
	}
	public class SQLiteHelper extends SQLiteOpenHelper {
		static final int k = 1;

		public SQLiteHelper(Context context, String name,
				CursorFactory factory, int version) {

			super(context, name, factory, k);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			db.execSQL(Databasevalues.SCRIPT_CREATE_TABLE_BAR_CODE);			
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS");
			onCreate(db);
		}

	}   
	public long insert_data(String username, String codedata, String date,String format) {
		// TODO Auto-generated method stub		
		ContentValues newuser = new ContentValues();		   	       		
		newuser.put(Databasevalues.USER_NAME, username.trim());		
		newuser.put(Databasevalues.BAR_CODE_DATA, codedata);
		newuser.put(Databasevalues.DATEANDTIME, date);
		newuser.put(Databasevalues.FORMAT, format);
		return sqLiteDatabase.insert(Databasevalues.TABLE_Name, null,
				newuser);

	}



	public void updatedata(String data,String format,int Userid)
	{
		//SQLiteDatabase db = c.getWritableDatabase();
		  Log.e("UseridUserid","UseridUserid"+ Userid);
		  Log.e("formatformat","formatformat"+ format);
		  Log.e("datadata","datadatadata"+ data);		 
		  ContentValues values = new ContentValues();
		  values.put(Databasevalues.BAR_CODE_DATA, data);
		  values.put(Databasevalues.FORMAT, format);
		  sqLiteDatabase.update(Databasevalues.TABLE_Name, values, Databasevalues.USER_ID+"="+Userid, null);
	}
	public void updatename(String name,String fname,int Userid)
	{
		//SQLiteDatabase db = c.getWritableDatabase();
		  //Log.e("UseridUserid","UseridUserid"+ Userid);		
		  ContentValues values = new ContentValues();
		  values.put(Databasevalues.USER_NAME, name.trim());		 
		  sqLiteDatabase.update(Databasevalues.TABLE_Name, values, Databasevalues.USER_ID+"="+Userid, null);
	}
	public void Deleterecord() {
		// TODO Auto-generated method stub	
		
		sqLiteDatabase.execSQL("delete from "+ Databasevalues.TABLE_Name);
		
	}
	public Cursor getallrecords() {
		// TODO Auto-generated method stub
		String readtemplate = "Select * from "+Databasevalues.TABLE_Name +"";
		Log.v("Select Query",""+ readtemplate);
		Cursor readtemp = sqLiteDatabase.rawQuery(readtemplate, null);
		return readtemp;
	}
		
}
