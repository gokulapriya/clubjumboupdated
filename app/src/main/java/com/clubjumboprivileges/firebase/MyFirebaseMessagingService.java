package com.clubjumboprivileges.firebase;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.clubjumboprivileges.home.ActivityNavigation;
import com.clubjumboprivileges.home.ActivitySplash;
import com.clubjumboprivileges.home.FragmentNotification;
import com.clubjumboprivileges.home.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

public class MyFirebaseMessagingService extends FirebaseMessagingService {


    String title,message;
    String imageUri = "";
    String fburl = "";
    Bitmap bitmap;
    String TAG = "FirebaseMessaging";

    final int NOTIFICATION_ID  = 1 ;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        //Calling method to generate icon_notification
        Log.d(TAG, "onMessageReceived: " + remoteMessage.getNotification());

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        if (remoteMessage != null) {
            if (remoteMessage.getData().get("text") != null) {
                message = remoteMessage.getData().get("text");
            }
            if (remoteMessage.getData().get("image") != null) {
                imageUri = remoteMessage.getData().get("image");
            }
            if (remoteMessage.getData().get("title") != null) {
                title = remoteMessage.getData().get("title");
            }
            if (remoteMessage.getData().get("url_link") != null) {
                fburl = remoteMessage.getData().get("url_link");
            }


        }

        bitmap = getBitmapfromUrl(imageUri);

        if (message.length() < 50){
            if (remoteMessage.getData().get("image") == null||remoteMessage.getData().get("image").equals("")){
                sendSmallTextNotification(message, title, fburl);
                Log.i("modurl",fburl);
            }
            else {
                showPictureNotification(bitmap,title,message,fburl);
                Log.i("modurl",fburl);

            }
        }
        else {
            if (remoteMessage.getData().get("image") == null||remoteMessage.getData().get("image").equals("")){
                sendBigTextNotification(message,title,fburl);
                Log.i("modurl",fburl);
            }
            else {
                showPictureNotification(bitmap,title,message,fburl);
                Log.i("modurl",fburl);

            }
        }

        /*if (remoteMessage.getData().get("image") == null||remoteMessage.getData().get("image").equals("")){

            if (message.length() > 50){
                sendBigTextNotification(message,title,fburl);
            }
            else {
                sendSmallTextNotification(message, title, fburl);
                Log.i("modurl",fburl);
            }
        }
        else {
            showPictureNotification(bitmap,title,message,fburl);
            Log.i("modurl",fburl);
        }*/


    }

    private void sendBigTextNotification(String message, String title, String fburl) {

        int importance = NotificationManager.IMPORTANCE_HIGH;
      /*  Intent intent = new Intent(this, ActivitySplash.class);
        intent.putExtra("isFromNotification", true);*/
        String channelId = "Club_Jumbo";
        String channelName = "Common Notification";

        RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.notification_small);
        RemoteViews notificationLayoutExpanded = new RemoteViews(getPackageName(), R.layout.notification_big_text);
        int NOTIFICATION_ID  =1;
// Apply the layouts to the notification

        Intent radio=new Intent(this, ActivitySplash.class);
        radio.putExtra("firebaseurl", fburl);//if necessary
        Log.i("firbaseimageurl",fburl);

        PendingIntent pRadio = PendingIntent.getActivity(this, 0, radio, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent switchIntent = new Intent(this, ActivityNavigation.switchButtonListener.class);
        switchIntent.putExtra("notificationId",NOTIFICATION_ID);

        PendingIntent pendingSwitchIntent = PendingIntent.getBroadcast(this, 1, switchIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Action viewAction = new NotificationCompat.Action.Builder(
                android.R.drawable.sym_action_chat, "VIEW", pRadio)
                .build();

        NotificationCompat.Action dismissAction = new NotificationCompat.Action.Builder(
                android.R.drawable.ic_menu_close_clear_cancel, "DISMISS", pendingSwitchIntent)
                .build();


        notificationLayout.setTextViewText(R.id.textTitle,title);
        notificationLayout.setTextViewText(R.id.textDescription,message);
        notificationLayoutExpanded.setTextViewText(R.id.textTitle,title);
        notificationLayoutExpanded.setTextViewText(R.id.textBigText,message);


       /* notificationLayoutExpanded.setOnClickPendingIntent(R.id.textDismiss,pendingSwitchIntent);
        notificationLayoutExpanded.setOnClickPendingIntent(R.id.textView,pRadio);*/
        notificationLayoutExpanded.setOnClickPendingIntent(R.id.relativeContent,pRadio);


        Notification customNotification = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.club_notify_small_icon)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(notificationLayout)
                .setCustomBigContentView(notificationLayoutExpanded)
                .addAction(viewAction)
                .addAction(dismissAction)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentIntent(pRadio)
                .build();

        NotificationManager mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            mNotificationManager.createNotificationChannel(mChannel);
        }
        mNotificationManager.notify(NOTIFICATION_ID ,customNotification);

    }

    private void showPictureNotification(Bitmap bitmap, String title, String message, String fburl) {

        int importance = NotificationManager.IMPORTANCE_HIGH;
      /*  Intent intent = new Intent(this, ActivitySplash.class);
        intent.putExtra("isFromNotification", true);*/
        String channelId = "Club_Jumbo";
        String channelName = "Common Notification";

        RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.notification_small);
        RemoteViews notificationLayoutExpanded = new RemoteViews(getPackageName(), R.layout.notification_large);
        int NOTIFICATION_ID  =1;
// Apply the layouts to the notification

        Intent radio=new Intent(this, ActivitySplash.class);
        radio.putExtra("firebaseurl", fburl);//if necessary
        Log.i("firbaseimageurl",fburl);

        PendingIntent pRadio = PendingIntent.getActivity(this, 0, radio, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent switchIntent = new Intent(this, ActivityNavigation.switchButtonListener.class);
        switchIntent.putExtra("notificationId",NOTIFICATION_ID);

        PendingIntent pendingSwitchIntent = PendingIntent.getBroadcast(this, 1, switchIntent, PendingIntent.FLAG_ONE_SHOT);


        NotificationCompat.Action viewAction = new NotificationCompat.Action.Builder(
                android.R.drawable.sym_action_chat, "VIEW", pRadio)
                .build();

        NotificationCompat.Action dismissAction = new NotificationCompat.Action.Builder(
                android.R.drawable.ic_menu_close_clear_cancel, "DISMISS", pendingSwitchIntent)
                .build();


        notificationLayout.setTextViewText(R.id.textTitle,title);
        notificationLayout.setTextViewText(R.id.textDescription,message);
        notificationLayoutExpanded.setTextViewText(R.id.textTitle,title);
        notificationLayoutExpanded.setTextViewText(R.id.textDescription,message);
        notificationLayoutExpanded.setImageViewBitmap(R.id.imgContent,bitmap);


        /*notificationLayoutExpanded.setOnClickPendingIntent(R.id.textToast,pendingSwitchIntent);
        notificationLayoutExpanded.setOnClickPendingIntent(R.id.textView,pRadio);*/
        notificationLayoutExpanded.setOnClickPendingIntent(R.id.relativeContent,pRadio);


        Notification customNotification = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.club_notify_small_icon)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(notificationLayout)
                .setCustomBigContentView(notificationLayoutExpanded)
                .addAction(viewAction)
                .addAction(dismissAction)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentIntent(pRadio)
                .build();

        NotificationManager mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            mNotificationManager.createNotificationChannel(mChannel);
        }
        mNotificationManager.notify(NOTIFICATION_ID ,customNotification);

    }

    private void sendSmallTextNotification(String message, String title, String fburl) {

        int importance = NotificationManager.IMPORTANCE_HIGH;
//        Intent intent = new Intent(this, ActivitySplash.class);
//        intent.putExtra("isFromNotification", true);
        String channelId = "Club_Jumbo";
        String channelName = "Common Notification";

        int NOTIFICATION_ID  =1;

        RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.notification_small);
        //RemoteViews notificationLayoutExpanded = new RemoteViews(getPackageName(), R.layout.notification_large);

// Apply the layouts to the notification

        Intent switchIntent = new Intent(this, ActivityNavigation.switchButtonListener.class);
        switchIntent.putExtra("notificationId",NOTIFICATION_ID);

        Intent radio=new Intent(this, ActivitySplash.class);
        radio.putExtra("firebaseurl", fburl);//if necessary
        Log.i("firbaseimageurl",fburl);
        PendingIntent pRadio = PendingIntent.getActivity(this, 0, radio, PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pendingSwitchIntent = PendingIntent.getBroadcast(this, 1, switchIntent, PendingIntent.FLAG_ONE_SHOT);


        notificationLayout.setTextViewText(R.id.textTitle,title);
        notificationLayout.setTextViewText(R.id.textDescription,message);
        notificationLayout.setOnClickPendingIntent(R.id.relativeContent,pRadio);

        NotificationCompat.Action viewAction = new NotificationCompat.Action.Builder(
                android.R.drawable.sym_action_chat, "VIEW", pRadio)
                .build();

        NotificationCompat.Action dismissAction = new NotificationCompat.Action.Builder(
                android.R.drawable.ic_menu_close_clear_cancel, "DISMISS", pendingSwitchIntent)
                .build();


        Notification customNotification = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.club_notify_small_icon)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(notificationLayout)
                .addAction(viewAction)
                .addAction(dismissAction)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentIntent(pRadio)
                .build();

        NotificationManager mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            mNotificationManager.createNotificationChannel(mChannel);
        }
        mNotificationManager.notify(NOTIFICATION_ID ,customNotification);


    }



    private void getNotification(RemoteMessage remoteMessage) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ((ActivityNavigation)getApplicationContext()).push(new FragmentNotification());
        }
       /* Intent intent = new Intent(this, ActivitySplash.class);
        intent.putExtra("isFromNotification", true);*/
      //  PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        String channelId = "Club_Jumbo";
        String channelName = "Common Notification";
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.logo_notifi);
        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
        notificationBuilder.setContentTitle(remoteMessage.getData().get("title")!=null?remoteMessage.getData().get("title"):"");
        notificationBuilder.setContentText(remoteMessage.getData().get("text")!=null?remoteMessage.getData().get("text"):"");
        notificationBuilder.setAutoCancel(true);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder.setSound(sound);
        notificationBuilder.setLargeIcon(largeIcon);
        notificationBuilder.setSmallIcon(R.mipmap.logo_notifi);
       // notificationBuilder.setContentIntent(pendingIntent);

        int notificationID = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        notificationManager.notify(notificationID, notificationBuilder.build());
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        if(imageUrl != null && !imageUrl.equals("")){
            try {
                URL url = new URL(""+imageUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(input);
                Bitmap resized = Bitmap.createScaledBitmap(bitmap, 1024, 512, true);
                resized.compress(Bitmap.CompressFormat.PNG,100,stream);

                return resized;

            } catch (Exception e) {
                e.printStackTrace();
                return null;

            }
        }
        return null;

    }
}