package com.clubjumboprivileges.firebase;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;



public class MyFirebaseInstanceIdServices extends FirebaseInstanceIdService
{
    public static final String REG_TOKEN="REG_TOKEN";
    SharedPreferences preflogin;

    @Override
    public void onTokenRefresh() {
        String recent_token = FirebaseInstanceId.getInstance().getToken();
        Log.d(REG_TOKEN,recent_token);
        preflogin  = getSharedPreferences("mytoken", Context.MODE_PRIVATE);


        SharedPreferences.Editor editor = preflogin.edit();
        editor.putString("key", recent_token);
        editor.commit();
        editor.apply();
    }
}

