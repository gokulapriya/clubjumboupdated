package com.clubjumboprivileges.utils;


import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CheckingNetworkAvailable {

	Context ctx;
	Typeface Fontstyle1,Fontstyle2,Fontstyle3,Fontstyle4;
	public CheckingNetworkAvailable(Context ctxt)
	{
		ctx = ctxt;
		Fontstyle1 = Typeface.createFromAsset(ctx.getAssets(), "fonts/HelveticaNeueLTStd-Lt.otf");
		Fontstyle2 = Typeface.createFromAsset(ctx.getAssets(), "fonts/HelveticaNeueLTStd-Roman.otf");
		Fontstyle3 = Typeface.createFromAsset(ctx.getAssets(), "fonts/HelveticaNeueLTStd-Th.otf");
		Fontstyle4 = Typeface.createFromAsset(ctx.getAssets(), "fonts/HelveticaNeueLTStd-Bd.otf");
	}
	public boolean hasConnection() {
		ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(
				Context.CONNECTIVITY_SERVICE);

		NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiNetwork != null && wifiNetwork.isConnected()) {
			return true;
		}

		NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (mobileNetwork != null && mobileNetwork.isConnected()) {
			return true;
		}

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (activeNetwork != null && activeNetwork.isConnected()) {
			return true; 
		}

		return false;
	}


	public void Showalert()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle("Internet Connection");
		builder.setMessage("Please check your internet connection is available!");
		builder.setPositiveButton("OK", null);
		AlertDialog dialog = builder.show();
		TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);
		TextView titleView = (TextView)dialog.findViewById(ctx.getResources().getIdentifier("alertTitle", "id", "android"));
        if (titleView != null) {
            titleView.setGravity(Gravity.CENTER);
        }
		dialog.show();

	}

	
	public void setEdittexttypeface(int fno,EditText v)
	{
		if(fno==1)
		{
			v.setTypeface(Fontstyle1);	
		}else if(fno == 2)
		{
			v.setTypeface(Fontstyle2);	
		}else if(fno == 3)
		{
			v.setTypeface(Fontstyle3);	
		}else if(fno == 4)
		{
			v.setTypeface(Fontstyle4);	
		}

	}

	public void setTextviewtypeface(int fno,TextView v)
	{
		if(fno == 1)
		{
			v.setTypeface(Fontstyle1);	
		}else if(fno == 2)
		{
			v.setTypeface(Fontstyle2);	
		}else if(fno == 3)
		{
			v.setTypeface(Fontstyle3);	
		}else if(fno == 4)
		{
			v.setTypeface(Fontstyle4);	
		}

	}

	public void setButtontypeface(int fno,Button v)
	{		
		if(fno==1)
		{
			v.setTypeface(Fontstyle1);	
		}else if(fno ==2)
		{
			v.setTypeface(Fontstyle2);	
		}else if(fno == 3)
		{
			v.setTypeface(Fontstyle3);	
		}else if(fno == 4)
		{
			v.setTypeface(Fontstyle4);	
		}
	}



	





}

