package com.clubjumboprivileges.utils;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.clubjumboprivileges.home.R;


public class Alertmessage {

	
	public Alertmessage(final Activity con) {
		final Dialog alertDialog = new Dialog(con);
		alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alertDialog.setContentView(R.layout.customalert);
		alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));		
		alertDialog.show();		
		final  Button Yes = (Button)alertDialog.findViewById(R.id.Yes);
		final Button  No = (Button)alertDialog.findViewById(R.id.no); 
		TextView titletext = (TextView)alertDialog.findViewById(R.id.titletext);		
		titletext.setText("Votre carte n'est pas valide. Merci d'utiliser uniquement une carte du My Jumbo le club");
		No.setVisibility(View.GONE);
		Yes.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				alertDialog.dismiss();

			}
		});				


	}
	
	private void hideKeyboard(Activity con) {   
	    // Check if no view has focus:
	    View view = con.getCurrentFocus();
	    if (view != null) {
	        InputMethodManager inputManager = (InputMethodManager) con.getSystemService(Context.INPUT_METHOD_SERVICE);
	        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
	

}


