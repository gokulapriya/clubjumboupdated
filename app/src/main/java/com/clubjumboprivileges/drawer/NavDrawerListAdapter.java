package com.clubjumboprivileges.drawer;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clubjumboprivileges.home.R;
import com.clubjumboprivileges.utils.CheckingNetworkAvailable;



public class NavDrawerListAdapter extends BaseAdapter {
	
	private Context context;
	private ArrayList<NavDrawerItem> navDrawerItems;
	CheckingNetworkAvailable check;
	public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
		check = new CheckingNetworkAvailable(context);
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {		
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.navigation_menu, null);
        }
         
		LinearLayout Click = (LinearLayout)convertView.findViewById(R.id.click);
        TextView title = (TextView) convertView.findViewById(R.id.title);       
        check.setTextviewtypeface(3, title);
        ImageView imgIcon = (ImageView)convertView.findViewById(R.id.icon); 
        //imgIcon.setImageResource(navDrawerItems.get(position).getIcon()); 
       /* if(position==HomeActivity.curposition)
        {
        	Click.setBackgroundColor(Color.WHITE);
        	title.setTextColor(Color.parseColor("#F13306"));
        }else{
        	
        	Click.setBackgroundColor(Color.parseColor("#F13306"));
        	title.setTextColor(Color.WHITE);
        }*/
        title.setText(navDrawerItems.get(position).getTitle());
        
               
        return convertView;
	}

}
