package com.clubjumboprivileges.Retrofit;

import com.clubjumboprivileges.Model_class.FirebaseModel;
import com.clubjumboprivileges.Model_class.NotificationModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ramkumar on 2/8/18.
 */

public interface APIinterface
{
    @FormUrlEncoded
    @POST("savefcm")
    Call<FirebaseModel>savefcm(@Field("user_id") String user_id,@Field("fcm") String fcm,@Field("platform") String platform);

    @FormUrlEncoded
    @POST("notificationlist")
    Call<NotificationModel>notification(@Field("type") Integer type, @Field("offset") Integer offset);

}
