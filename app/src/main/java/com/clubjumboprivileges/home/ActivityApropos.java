package com.clubjumboprivileges.home;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clubjumboprivileges.utils.UtilsDefault;

public class ActivityApropos extends AppCompatActivity {


    ProgressDialog progressDialog;
    TextView textView;
    ImageView backBtn;
    TextView textHeader;
    WebView webviewApropos;
    RelativeLayout relativeNointernet;
    String fontcolor ="<font color='#000000'>";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apropos);

        progressDialog=new ProgressDialog(this);
        relativeNointernet = findViewById(R.id.relativeNointernet);
        webviewApropos = findViewById(R.id.webviewApropos);
        backBtn=(ImageView)findViewById(R.id.backBtn);
        textView=(TextView)findViewById(R.id.apro);
        textHeader = findViewById(R.id.textHeader);
        textHeader.setText(R.string.apropos);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#000000"));
        }


        if(isInternetOn()) {

            relativeNointernet.setVisibility(View.GONE);
            webviewApropos.setVisibility(View.VISIBLE);

            progressDialog.setMessage(getString(R.string.loading));
            webviewApropos.loadUrl("https://jumbo.mu/somags-minipage");
            WebSettings webSettings = webviewApropos.getSettings();
            webSettings.setJavaScriptEnabled(true);
            progressDialog.show();
        }
        else
        {
            relativeNointernet.setVisibility(View.VISIBLE);
            webviewApropos.setVisibility(View.GONE);

            Toast.makeText(this,  R.string.no_internet, Toast.LENGTH_SHORT).show();
        }

        webviewApropos.setWebViewClient(new WebviewApropos());
        webviewApropos.setWebChromeClient(new WebChromeClient());



        String abouttext =fontcolor+ "Somags est depuis de nombreuses années, l’un des principaux acteurs historique du commerce alimentaire à Maurice." +"</font>"+
                "<br>"+"<br>"+fontcolor+"Somags est une filiale du Groupe Casino, l'un des leaders de la grande distribution présent dans le monde entier. Le Groupe Casino exploite plus de 11.000 points de vente, emploie quelque 307.000 personnes et génère un chiffre d'affaires annuel de plus de 41 milliards d'euros en 2012." +"</font>"+"<br>"+"<br>"+
                fontcolor+"Avec un positionnement multi format au travers des enseignes d’hypermarchés (Jumbo Score) et de magasins de proximité (Supermarchés Spar), Somags reste incontestablement le pionnier. Le premier supermarché Spar a ouvert ses portes en Septembre 1993, avec une offre répondant aux besoins alimentaires au jour le jour. Aujourd’hui, Spar compte six supermarchés à travers l'Ile." +"</font>"+"<br>"+"<br>"+
                fontcolor+"Avec l’ouverture en septembre 1994 du premier hypermarché \"Continent\", SOMAGS créé l'évènement. Les mauriciens sont tout de suite séduits et découvrent une nouvelle façon de faire leurs courses dans un concept intégré : tout sous un même toit ! Produits frais, boulangerie, pâtisserie, fruits & légumes, viande, produits alimentaires et non-alimentaires, parking, galerie marchande, loisirs..." +"</font>"+"<br>"+"<br>"+
                fontcolor+"Aout 2003, l’histoire continue avec l'ouverture d'un deuxième centre commercial à Riche-Terre. Depuis, ces deux centres commerciaux s’imposent comme des destinations familiales à la mode pour l’alimentation, le shopping et les loisirs." +"</font'>"+"<br>"+"<br>"+
                fontcolor+"Après toutes ces années, Somags est toujours fidèle à ses valeurs et à ses engagements : être attentif aux besoins des consommateurs mauriciens, offrir les meilleurs produits et services ainsi qu’un excellent rapport qualité/prix. Conscient des conditions économiques difficiles actuelles, Somags accentue la mise en avant de promotions et lance sa marque «Casino» pour offrir des produits de qualité internationale à un prix très concurrentiel.";
      //  textView.setText(Html.fromHtml(abouttext));
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onBackPressed();
            }
        });
    }

    public class WebviewApropos extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

    public final boolean isInternetOn() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm =(ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
