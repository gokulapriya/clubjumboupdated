package com.clubjumboprivileges.home;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clubjumboprivileges.database.DatabaseHandler;
import com.clubjumboprivileges.utils.Alertdialog;
import com.clubjumboprivileges.utils.Alertmessage;
import com.clubjumboprivileges.utils.CheckingNetworkAvailable;



public class FragmentManuallycreate extends Fragment {

	private Button scan;
	EditText statustext;
	DatabaseHandler dbh;
	Alertdialog alertdialog;
	SharedPreferences pref;
	Editor edit;
	String content = "";
	CheckingNetworkAvailable check;
	LinearLayout commonlayout;	
	Alertmessage alertmsg;
	ImageView backBtn;
	TextView textHeader;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_manualcreate, container, false);

		init(rootView);

		scan.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				content = statustext.getText().toString().trim();
				if(content.equals(""))
				{
					Toast.makeText(getActivity(), " Le champ du code à barres ne doit pas être vide", Toast.LENGTH_LONG).show();
				}else
				if(content.length()<12)
				{
					alertmsg = new Alertmessage(getActivity());
				}
				else{
					Intent in=new Intent("settype");
					in.putExtra("name",1);
					getActivity().sendBroadcast(in);
					edit.putString("CONTENTS", content);
					edit.putString("FORMAT", "CODE_128");
					edit.commit();
					Fragment fragment = null;
					fragment = new FragmentScandetails();
					Bundle bundle = new Bundle();
					bundle.putString("cont",statustext.getText().toString());
					fragment.setArguments(bundle);
					FragmentManager fragmentManager = getActivity().getFragmentManager();
					fragmentManager.beginTransaction()
							.addToBackStack("FragmentScandetails")
							.replace(R.id.containerLayout, fragment).commit();
				}
			}
		});
		backBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().onBackPressed();
			}
		});
		return rootView;
	}

	private void init(View rootView) {
		check = new CheckingNetworkAvailable(getActivity());
		commonlayout = (LinearLayout)rootView.findViewById(R.id.commonlayout);
		scan= (Button)rootView.findViewById(R.id.createqr);		
		statustext = (EditText)rootView.findViewById(R.id.qrtext1);
		dbh = new DatabaseHandler(getActivity());
		pref = getActivity().getSharedPreferences("LOYALTY", Context.MODE_PRIVATE);
		edit = pref.edit();	
		check.setButtontypeface(4, scan);
		backBtn=(ImageView)rootView.findViewById(R.id.backBtn);
		textHeader = rootView.findViewById(R.id.textHeader);
		textHeader.setText(R.string.personalisation);

	}

}

