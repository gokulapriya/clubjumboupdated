package com.clubjumboprivileges.home;


import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPartenaires extends Fragment {

    ProgressDialog progressDialog;
    WebView webPartenaries;
   /* ImageView backBtn;
    TextView textHeader;*/
    RelativeLayout relativeNointernet;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_partenaries, container, false);
        progressDialog=new ProgressDialog(getActivity());
        webPartenaries = view.findViewById(R.id.webPartenaries);
       // backBtn=(ImageView) view.findViewById(R.id.backBtn);
        relativeNointernet = view.findViewById(R.id.relativeNointernet);
        ((ActivityNavigation)getActivity()).imgHeader.setVisibility(View.GONE);
        ((ActivityNavigation)getActivity()).textHeader.setVisibility(View.VISIBLE);
        ((ActivityNavigation)getActivity()).textHeader.setText(R.string.nos_partenaries);
        ((ActivityNavigation)getActivity()).notification.setVisibility(View.VISIBLE);
        ((ActivityNavigation)getActivity()).fragmentPosition=1;
        /*textHeader = view.findViewById(R.id.textHeader);
        textHeader.setText(R.string.nos_partenaries);*/
        initializeListener();
        getData();

        return view;
    }
    private void initializeListener() {
       /* backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webPartenaries.canGoBack()){
                    webPartenaries.goBack();
                }
                else{

                }
            }
        });*/

    }

    private void getData() {
        if(isInternetOn()) {

            relativeNointernet.setVisibility(View.GONE);
            webPartenaries.setVisibility(View.VISIBLE);

            progressDialog.setMessage(getString(R.string.loading));
            webPartenaries.loadUrl("https://jumbo.mu/my-jumbo-le-club/nos-partenaires-minipage");
            WebSettings webSettings = webPartenaries.getSettings();
            webSettings.setLoadsImagesAutomatically(true);
            webSettings.setDomStorageEnabled(true);
            webPartenaries.setWebViewClient(new webviewfragement());
            webPartenaries.setWebChromeClient(new WebChromeClient());
            webSettings.setJavaScriptEnabled(true);
            progressDialog.show();
        }
        else
        {
            relativeNointernet.setVisibility(View.VISIBLE);
            webPartenaries.setVisibility(View.GONE);

            Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }


    }

    public class webviewfragement extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }
    public final boolean isInternetOn() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm =(ConnectivityManager)getActivity().getSystemService(getActivity().getBaseContext().CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}
