package com.clubjumboprivileges.home;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.SslError;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityNotification extends AppCompatActivity {

    ProgressDialog progressDialog;
    TextView textView,textHeader;
    ImageView backBtn;
    WebView webviewMention;
    RelativeLayout relativeNointernet;
    String urlLink = "" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_webview);

        progressDialog=new ProgressDialog(this);
        textView=(TextView) findViewById(R.id.amenss);
        textHeader = (TextView) findViewById(R.id.textHeader);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        relativeNointernet = findViewById(R.id.relativeNointernet);
        webviewMention = findViewById(R.id.webviewMention);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            urlLink = extras.getString("firebaseurl");
            // and get whatever type user account id is
        }

         textHeader.setText(R.string.notification);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if(isInternetOn() && urlLink!=null) {

            relativeNointernet.setVisibility(View.GONE);
            webviewMention.setVisibility(View.VISIBLE);

            progressDialog.setMessage(getString(R.string.loading));
            webviewMention.loadUrl(urlLink);
            WebSettings webSettings = webviewMention.getSettings();
            webSettings.setJavaScriptEnabled(true);
            progressDialog.show();
            webviewMention.setWebViewClient(new WebviewMention());
        }
        else
        {
            relativeNointernet.setVisibility(View.VISIBLE);
            webviewMention.setVisibility(View.GONE);
            Toast.makeText(this,  R.string.no_internet+" No valid url", Toast.LENGTH_SHORT).show();
        }



    }

    public class WebviewMention extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            handler.proceed();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

    public final boolean isInternetOn() {

        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm =(ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ActivityNotification.this, ActivityNavigation.class);
        startActivity(i);
        finish();
    }
}
