package com.clubjumboprivileges.home;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityPromotions extends AppCompatActivity {
    ProgressDialog progressDialog;
    WebView webViewPromotions;
    ImageView backBtn;
    TextView textHeader;
    LinearLayout linearTool;
    RelativeLayout relativeNointernet;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotions);

        progressDialog=new ProgressDialog(this);
        webViewPromotions = (WebView)findViewById(R.id.wvv1);
        relativeNointernet = findViewById(R.id.relativeNointernet);
        linearTool = findViewById(R.id.linearTool);
        linearTool.setVisibility(View.VISIBLE);
        textHeader = findViewById(R.id.textHeader);
        textHeader.setText(R.string.nos_promotions);
        backBtn=(ImageView)findViewById(R.id.backBtn);

        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.parseColor("#000000"));
            }
            getSupportActionBar().hide();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        if(isInternetOn()) {

            relativeNointernet.setVisibility(View.GONE);
            webViewPromotions.setVisibility(View.VISIBLE);

            progressDialog.setMessage(getString(R.string.loading));
            WebSettings webSettings = webViewPromotions.getSettings();
            webSettings.setJavaScriptEnabled(true);
            progressDialog.show();
            webSettings.setLoadsImagesAutomatically(true);
            webSettings.setDomStorageEnabled(true);
            webViewPromotions.loadUrl("https://jumbo.mu/promominipage");
            webViewPromotions.setWebViewClient(new WebviewPromotions());
            webViewPromotions.setWebChromeClient(new WebChromeClient());
        }
        else {
            relativeNointernet.setVisibility(View.VISIBLE);
            webViewPromotions.setVisibility(View.GONE);
            Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }


        Intent intent =getIntent();
        intent.putExtra("firebase","9");
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webViewPromotions.canGoBack()){
                    webViewPromotions.goBack();
                }
                else{
                    onBackPressed();
                }
            }
        });

    }
    public class WebviewPromotions extends WebViewClient
    {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        progressDialog.dismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ( progressDialog!=null && progressDialog.isShowing() ){
            progressDialog.cancel();
        }
    }

    public final boolean isInternetOn() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm =(ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    @Override
    public void onBackPressed() {
        if (webViewPromotions.canGoBack()){
            webViewPromotions.goBack();
        }
        else{
            super.onBackPressed();
        }
    }
}