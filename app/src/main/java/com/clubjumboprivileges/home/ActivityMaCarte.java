package com.clubjumboprivileges.home;


import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ActivityMaCarte extends AppCompatActivity {

   ImageView backBtn;
   RelativeLayout relativeScan;
   RelativeLayout relativeEmail;
   TextView textHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_macarte);

        backBtn=(ImageView)findViewById(R.id.backBtn);
        relativeScan = findViewById(R.id.relative_scan);
        relativeEmail = findViewById(R.id.relative_email);
        textHeader = findViewById(R.id.textHeader);
        textHeader.setText(R.string.macarte);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#000000"));
        }



        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        relativeScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ActivityScan.class);
                startActivity(intent);
            }
        });

        relativeEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             Intent intent =new Intent(getApplicationContext(),ActivityEmailsign.class);
             startActivity(intent);

            }
        });

    }
}
