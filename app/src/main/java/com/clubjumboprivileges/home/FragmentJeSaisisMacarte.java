package com.clubjumboprivileges.home;




import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clubjumboprivileges.database.DatabaseHandler;
import com.clubjumboprivileges.utils.Alertmessage;
import com.clubjumboprivileges.utils.Permission;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;



public class FragmentJeSaisisMacarte extends Fragment {

	ImageView backBtn;
	RelativeLayout relativeScan;
	RelativeLayout relativeManual;
	DatabaseHandler dbh;
	EditText firstname;
	String contents;
	String format;
	SharedPreferences preference;
	Editor edit;
	Alertmessage alertmsg;
	TextView textHeader;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_jesaisismacarte, container, false);
		Permission.verifyStoragePermissions(getActivity());
		preference = getActivity().getSharedPreferences("LOYALTY", Context.MODE_PRIVATE);	
		edit = preference.edit();		        
		relativeScan = rootView.findViewById(R.id.relative_scan);
		relativeManual = rootView.findViewById(R.id.relative_manual);
		backBtn=(ImageView)rootView.findViewById(R.id.backBtn);
		textHeader = rootView.findViewById(R.id.textHeader);
		textHeader.setText("Je saisis ma carte");
		dbh = new DatabaseHandler(getActivity());		
		
		Intent in=new Intent("settype");
		in.putExtra("name", 2);
		getActivity().sendBroadcast(in);


		backBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().onBackPressed();
			}
		});
		relativeScan.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				IntentIntegrator init=IntentIntegrator.forFragment(FragmentJeSaisisMacarte.this);
				init.setOrientationLocked(false);
				init.setCaptureActivity(Barcodescanneractivity.class);
				init.initiateScan();
			}
		});
		relativeManual.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in=new Intent("settype");
				in.putExtra("name", 0);
				getActivity().sendBroadcast(in);
				Fragment fragment = null;
				fragment = new FragmentManuallycreate();
				FragmentManager fragmentManager = getActivity().getFragmentManager();
				fragmentManager.beginTransaction()
						.addToBackStack("FragmentManuallycreate")
				.replace(R.id.containerLayout, fragment).commit();

			}
		});

		return rootView;
	}
	@Override
	public  void onActivityResult(int requestCode, int resultCode, Intent data) {
		IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
		if(result != null) {
			if(result.getContents() == null) {
			} else {
				contents=result.getContents();
				format=result.getFormatName();

				if(contents.length()<5)
				{
					alertmsg = new Alertmessage(getActivity());
				}else if(contents.length()>13)
				{
					alertmsg = new Alertmessage(getActivity());
				}else{
					edit.putString("CONTENTS", contents);
					edit.putString("FORMAT", format);
					edit.commit();
					Fragment fragment = null;
					fragment = new FragmentScandetails();
					FragmentManager fragmentManager = getActivity().getFragmentManager();
					fragmentManager.beginTransaction().replace(R.id.containerLayout, fragment).commit();
				}


			}
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
}
