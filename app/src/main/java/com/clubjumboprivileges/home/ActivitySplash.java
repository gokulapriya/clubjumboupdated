package com.clubjumboprivileges.home;


import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import java.io.File;


public class ActivitySplash extends AppCompatActivity {

    SharedPreferences pref;
    SharedPreferences sharedpreferencesBarcode;
    Editor edit;
    Intent intent;
    int id;
    boolean isFromNotification=false;
    String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for sInstance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        if (getIntent() != null) {
            url = "";
            Intent intent = getIntent();
            url = intent.getStringExtra("firebaseurl");
        }
        if(url != null){
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(1);
            Intent intent = new Intent(this,ActivityNotification.class);
            intent.putExtra("firebaseurl",url);
            Log.i("firebaseurl",url);
            startActivity(intent);
            finish();
        }
        else {
            int SPLASH_TIME_OUT = 2000;
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                /*Log.e("StatusStatus", "StatusStatusStatus" +
                        pref.getBoolean("Status", false));*/
                    Intent i = new Intent(ActivitySplash.this, ActivityNavigation.class);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }






        /*sharedpreferencesBarcode = getSharedPreferences("BAR", Context.MODE_PRIVATE);
        pref = getSharedPreferences("LOYALTY", Context.MODE_PRIVATE);
        edit = pref.edit();
        if (getIntent() != null) {
            Intent intent = getIntent();
             isFromNotification= intent.getBooleanExtra("isFromNotification", false);
        }

        if (isFromNotification) {
            Intent intent2 = new Intent(ActivitySplash.this, ActivityPromotions.class);
            startActivity(intent2);
            finish();
        } else {
            int SPLASH_TIME_OUT = 2000;
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Log.e("StatusStatus", "StatusStatusStatus" +
                            pref.getBoolean("Status", false));
                    Intent i = new Intent(ActivitySplash.this, ActivityNavigation.class);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }*/
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) { e.printStackTrace();}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // deleteCache(this);
    }
}
