package com.clubjumboprivileges.home;


import android.app.ProgressDialog;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clubjumboprivileges.Model_class.NotificationModel;
import com.clubjumboprivileges.Retrofit.APIClient;
import com.clubjumboprivileges.Retrofit.APIinterface;
import com.clubjumboprivileges.adapter.NotificationAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNotification extends Fragment {

    ProgressDialog progressDialog;
    ImageView backBtn;
    TextView textHeader,tvNoNotification;
    RelativeLayout relativeNointernet,rtNotification;
    RecyclerView rvNotification;
    int limit=10;
    int offset=0;
    ArrayList<NotificationModel.resulst> notificationList = new ArrayList<>();
    NotificationAdapter notificationAdapter;
    boolean loading=false;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_notification, container, false);


        progressDialog=new ProgressDialog(getContext());
        rvNotification = view.findViewById(R.id.rv_notification);
        tvNoNotification = view.findViewById(R.id.tv_no_notification);
        relativeNointernet = view.findViewById(R.id.relativeNointernet);
        rtNotification = view.findViewById(R.id.rt_notification);

        //initializeListener();
        ((ActivityNavigation)getActivity()).imgHeader.setVisibility(View.GONE);
        ((ActivityNavigation)getActivity()).textHeader.setVisibility(View.VISIBLE);
        ((ActivityNavigation)getActivity()).textHeader.setText(R.string.notification);
        ((ActivityNavigation)getActivity()).fragmentPosition=1;
        ((ActivityNavigation)getActivity()).notification.setVisibility(View.GONE);

        rvNotification.setHasFixedSize(true);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        rvNotification.setLayoutManager(mLayoutManager);
        rvNotification.setItemAnimator(new DefaultItemAnimator());
        rvNotification.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = ((LinearLayoutManager) mLayoutManager).findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset=offset+limit;
                            getNotificationData();
                        }
                    }
                }
            }
        });
        getNotificationData();

        return view;
    }


    public void getNotificationData() {
        if(isInternetOn()) {

        relativeNointernet.setVisibility(View.GONE);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();

        APIinterface apiService = APIClient.getClient().create(APIinterface.class);
        Call<NotificationModel> call = apiService.notification(1,offset);
        call.enqueue(new Callback<NotificationModel>() {
            @Override
            public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {
                progressDialog.dismiss();
                Gson gson = new Gson();
                System.out.println("Response -->"+ gson.toJson(response.body()));
                if (response.body() != null) {
                    if (response.body().getResulst() != null && response.body().getResulst().size() != 0) {
                        if (response.body().getResulst().size() > 0) {
                            notificationList.addAll(response.body().getResulst());
                        }

                        if(notificationList.size() > 0){
                            rvNotification.setVisibility(View.VISIBLE);
                            tvNoNotification.setVisibility(View.GONE);
                            notificationAdapter = new NotificationAdapter(notificationList,getContext());
                            rvNotification.setAdapter(notificationAdapter);
                            notificationAdapter.notifyDataSetChanged();
                            loading = true;
                            progressDialog.dismiss();
                        }else {
                            rvNotification.setVisibility(View.GONE);
                            tvNoNotification.setVisibility(View.VISIBLE);
                            progressDialog.dismiss();
                        }

                    } else {
                        //Toast.makeText(getContext(), "No Data Found", Toast.LENGTH_SHORT).show();
                        notificationList.clear();
                        progressDialog.dismiss();
                        loading = false;
                    }

                }
                else {
                Toast.makeText(getContext(), "Please try again After sometimes", Toast.LENGTH_SHORT).show();

            }
            }

            @Override
            public void onFailure(Call<NotificationModel> call, Throwable t) {
                //failurecase
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Failed to load", Toast.LENGTH_SHORT).show();
                System.out.println("Error"+ t.getMessage());
            }
        });
        }
        else
        {
            relativeNointernet.setVisibility(View.VISIBLE);
            rtNotification.setVisibility(View.GONE);

            Toast.makeText(getContext(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }

    }

    public final boolean isInternetOn() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm =(ConnectivityManager)getActivity().getSystemService(getActivity().getBaseContext().CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}
