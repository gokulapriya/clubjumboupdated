package com.clubjumboprivileges.home;


import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentApropos extends Fragment {

    ProgressDialog progressDialog;
    TextView textView;
    ImageView backBtn;
    TextView textHeader;
    WebView webviewApropos;
    RelativeLayout relativeNointernet;
    String fontcolor ="<font color='#000000'>";


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_apropos, container, false);

        progressDialog=new ProgressDialog(getActivity());
        relativeNointernet = view.findViewById(R.id.relativeNointernet);
        webviewApropos = view.findViewById(R.id.webviewApropos);
        /*backBtn=(ImageView) view.findViewById(R.id.backBtn);
        textView=(TextView) view.findViewById(R.id.apro);
        textHeader = view.findViewById(R.id.textHeader);
        textHeader.setText(R.string.apropos);*/
        ((ActivityNavigation)getActivity()).imgHeader.setVisibility(View.GONE);
        ((ActivityNavigation)getActivity()).textHeader.setVisibility(View.VISIBLE);
        ((ActivityNavigation)getActivity()).textHeader.setText(R.string.apropos);
        ((ActivityNavigation)getActivity()).notification.setVisibility(View.VISIBLE);
        ((ActivityNavigation)getActivity()).fragmentPosition=1;


        if(isInternetOn()) {

            relativeNointernet.setVisibility(View.GONE);
            webviewApropos.setVisibility(View.VISIBLE);

            progressDialog.setMessage(getString(R.string.loading));
            webviewApropos.loadUrl("https://jumbo.mu/somags-minipage");
            WebSettings webSettings = webviewApropos.getSettings();
            webSettings.setJavaScriptEnabled(true);
            progressDialog.show();
        }
        else
        {
            relativeNointernet.setVisibility(View.VISIBLE);
            webviewApropos.setVisibility(View.GONE);

            Toast.makeText(getActivity(),  R.string.no_internet, Toast.LENGTH_SHORT).show();
        }

        webviewApropos.setWebViewClient(new WebviewApropos());
        webviewApropos.setWebChromeClient(new WebChromeClient());

        return view;
    }

    public class WebviewApropos extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

    public final boolean isInternetOn() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm =(ConnectivityManager)getActivity().getSystemService(getActivity().getBaseContext().CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}
