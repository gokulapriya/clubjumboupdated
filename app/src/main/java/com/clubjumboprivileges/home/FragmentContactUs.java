package com.clubjumboprivileges.home;


import android.app.ProgressDialog;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clubjumboprivileges.utils.UtilsDefault;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentContactUs extends Fragment {


    ProgressDialog progressDialog;
    WebView webViewContact;
   /* ImageView backBtn;
    TextView textHeader;*/
    RelativeLayout relativeNointernet;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_contecter, container, false);

        progressDialog=new ProgressDialog(getActivity());
        relativeNointernet = view.findViewById(R.id.relativeNointernet);
       /* backBtn=(ImageView)findViewById(R.id.backBtn);
        textHeader = findViewById(R.id.textHeader);
        textHeader.setText(R.string.contact_nous);*/
        webViewContact = view.findViewById(R.id.wvv3);
        ((ActivityNavigation)getActivity()).imgHeader.setVisibility(View.GONE);
        ((ActivityNavigation)getActivity()).textHeader.setVisibility(View.VISIBLE);
        ((ActivityNavigation)getActivity()).textHeader.setText(R.string.contact_nous);
        ((ActivityNavigation)getActivity()).notification.setVisibility(View.VISIBLE);
        ((ActivityNavigation)getActivity()).fragmentPosition=1;

        if(isInternetOn()) {

            relativeNointernet.setVisibility(View.GONE);
            webViewContact.setVisibility(View.VISIBLE);

            progressDialog.setMessage(getString(R.string.loading));
            webViewContact.loadUrl("https://jumbo.mu/contactminipage");
            WebSettings webSettings = webViewContact.getSettings();
            webSettings.setJavaScriptEnabled(true);
            progressDialog.show();
        }
        else
        {
            relativeNointernet.setVisibility(View.VISIBLE);
            webViewContact.setVisibility(View.GONE);

            Toast.makeText(getActivity(), R.string.no_internet, Toast.LENGTH_SHORT).show();
        }
        webViewContact.setWebViewClient(new WebviewContactus());

        return view;
    }

    public class WebviewContactus extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }


    public final boolean isInternetOn() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm =(ConnectivityManager)getActivity().getSystemService(getActivity().getBaseContext().CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}
