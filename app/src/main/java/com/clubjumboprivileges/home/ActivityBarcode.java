package com.clubjumboprivileges.home;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.clubjumboprivileges.database.DatabaseHandler;
import com.clubjumboprivileges.database.Databasevalues;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.EnumMap;
import java.util.Map;

public class ActivityBarcode extends Activity {
    TextView username;
    ImageView barcode1;
    String firstName ="";
    TextView barcodenumber1;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    DatabaseHandler dbh1;
    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    Button deleteBtn;
    ImageView imgDelete;
    int height = 600;
    int width = 300;
    ImageView backBtn;
    TextView textHeader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);

        imgDelete = findViewById(R.id.img_delete);
        backBtn=(ImageView)findViewById(R.id.backBtn);
        /*textHeader = findViewById(R.id.textHeader);
        textHeader.setText("Personnalisation");*/

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#000000"));
        }

        try {
            init();
            loadDatas();
        }
        catch (WriterException e)
        {
            e.printStackTrace();
        }
        pref = getSharedPreferences("LOYALTY", Context.MODE_PRIVATE);
        edit = pref.edit();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(ActivityBarcode.this)
                            .setTitle("Êtes-vous sûr ?")
                            .setPositiveButton("Annuler", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();

                                        }
                            })
                            .setNegativeButton("Oui", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    edit.putInt("check", 0);
                                    edit.commit();
                                    finish();
                                    sendBroadcast(new Intent("scan"));
                                }
                            })

                     .setIcon(android.R.drawable.ic_dialog_alert)
                                                    .show();
                }
            });
        }
    private void init() {
        pref = getSharedPreferences("LOYALTY", Context.MODE_PRIVATE);
        edit = pref.edit();
        dbh1 = new DatabaseHandler(this);
        username = (TextView)findViewById(R.id.firstname22);
        barcode1 = (ImageView)findViewById(R.id.barcodeimage22);
        barcodenumber1 = (TextView)findViewById(R.id.barcodestring22);
        deleteBtn = (Button) findViewById(R.id.deleteBtn);

    }
    private void loadDatas() throws WriterException {
        dbh1.opendb();
        Cursor cursor = dbh1.getallrecords();
        if (cursor.moveToFirst()) {

            while (!cursor.isAfterLast()) {
                int userId = cursor.getInt(cursor
                        .getColumnIndex("User_id"));
                firstName = cursor.getString(cursor
                        .getColumnIndex(Databasevalues.USER_NAME));
                String barcodedata = cursor.getString(cursor
                        .getColumnIndex(Databasevalues.BAR_CODE_DATA));
                String format = cursor.getString(cursor
                        .getColumnIndex(Databasevalues.FORMAT));

                edit.putInt("Userid", userId);
                edit.commit();
                username.setText(firstName);
                barcodenumber1.setText(barcodedata);

                Bitmap bmp = null;

                if(!format.equals(""))
                {
                    if(format.equalsIgnoreCase("AZTEC"))
                    {
                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.AZTEC, height, width);

                    }else if(format.equalsIgnoreCase("CODABAR"))
                    {
                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.CODABAR, height, width);


                    }else if(format.equalsIgnoreCase("CODE_39"))
                    {
                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.CODE_39, height, width);


                    }else if(format.equalsIgnoreCase("CODE_93"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.CODE_93, height, width);

                    }else if(format.equalsIgnoreCase("CODE_128"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.CODE_128, height, width);

                    }else if(format.equalsIgnoreCase("DATA_MATRIX"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.DATA_MATRIX, height, width);
                        Drawable d = new BitmapDrawable(getResources(), bmp);
                        barcode1.setImageDrawable(d);
                    }else if(format.equalsIgnoreCase("EAN_8"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.EAN_8, height, width);

                    }else if(format.equalsIgnoreCase("EAN_13"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.EAN_13, height, width);


                    }else if(format.equalsIgnoreCase("ITF"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.ITF, height, width);

                    }else if(format.equalsIgnoreCase("MAXICODE"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.MAXICODE, height, width);

                    }
                    else if(format.equalsIgnoreCase("PDF_417"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.PDF_417, height, width);

                    }else if(format.equalsIgnoreCase("QR_CODE"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.QR_CODE, height, width);

                    }else if(format.equalsIgnoreCase("RSS_14"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.RSS_14, height, width);

                    }else if(format.equalsIgnoreCase("RSS_EXPANDED"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.RSS_EXPANDED, height, width);

                    }else if(format.equalsIgnoreCase("UPC_A"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.UPC_A, height, width);

                    }else if(format.equalsIgnoreCase("UPC_E"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.UPC_E, height, width);

                    }else if(format.equalsIgnoreCase("UPC_EAN_EXTENSION"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.UPC_EAN_EXTENSION, height, width);
                    }
                    Drawable d = new BitmapDrawable(getResources(), bmp);
                    int sdk = android.os.Build.VERSION.SDK_INT;
                    if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        barcode1.setImageDrawable(d);
                    } else {
                        barcode1.setImageDrawable(d);
                    }

                }
                cursor.moveToNext();
            }
        }
        dbh1.closeconnection();

    }

    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int imgWidth, int imgHeight) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, imgWidth, imgHeight, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int resultWidth = result.getWidth();
        int resultHeight = result.getHeight();
        int[] pixels = new int[resultWidth * resultHeight];
        for (int y = 0; y < resultHeight; y++) {
            int offset = y * resultWidth;
            for (int x = 0; x < resultWidth; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(resultWidth, resultHeight,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, resultWidth, 0, 0, resultWidth, resultHeight);
        return bitmap;
    }
    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }
}
