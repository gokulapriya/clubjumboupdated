package com.clubjumboprivileges.home;

import android.app.AlertDialog;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.clubjumboprivileges.Model_class.FirebaseModel;
import com.clubjumboprivileges.Retrofit.APIClient;
import com.clubjumboprivileges.Retrofit.APIinterface;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

public class ActivityNavigation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    SharedPreferences preflogin;
    String regId="";
    SharedPreferences sharedPreferences;
    ImageView img;
    ImageView img3;
    List<FirebaseModel.Result> model =new ArrayList<>();
    ImageView opennav,notification;
    String android_id;
    private  int value;
    NavigationView navigationView;
    private DrawerLayout drawer;
    ImageButton button;
    ImageView imgHeader;
    TextView textHeader;
    Toolbar toolbar;
    int fragmentPosition =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        /*try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.parseColor("#000000"));

            }
            getSupportActionBar().hide();
        }
        catch (Exception e){
            e.printStackTrace();
        }*/

        preflogin = getSharedPreferences("mytoken", MODE_PRIVATE);
        regId = preflogin.getString("key", "");

        navigationView  =  findViewById(R.id.nav_view);
        img =findViewById(R.id.imgpromo);
        opennav=findViewById(R.id.menu);
        notification=findViewById(R.id.notification);
        drawer =  findViewById(R.id.drawer_layout1);
        img3 = findViewById(R.id.imgcont);
        toolbar = findViewById(R.id.toolbar);
        imgHeader = findViewById(R.id.imgHeader);
        textHeader = findViewById(R.id.textHeader);

        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        button  =(ImageButton)headerView.findViewById(R.id.closei);

         android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d(android_id,"android_id");
        Log.e("regid",regId);




        opennav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.END);

            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                push(new FragmentNotification());

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawer.closeDrawers();
            }
        });

        sharedPreferences = getSharedPreferences("BAR", Context.MODE_PRIVATE);

        toolbar.setNavigationIcon(null);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setIcon(null);
         value=sharedPreferences.getInt("check",0);
        Log.d("FRIDAY","value"+value);
        displaySelectedScreen(R.id.nav_carte);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        savefcm();

    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            if (fragmentPosition!=0){
               push(new FragmentCarte());
               fragmentPosition =0;
            }
            else  {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.exit_application)
                        .setMessage("Are you sure want to Exit From this Application?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                popAllBackstack();
                                finishAffinity();
                                ActivityNavigation.super.onBackPressed();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }


        }

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        displaySelectedScreen(item.getItemId());
        return true;
    }
    private void displaySelectedScreen(int itemId) {

        android.support.v4.app.Fragment fragment = null;

        switch (itemId) {
            case R.id.nav_carte:
                fragmentPosition =0;
                push(new FragmentCarte());
               // fragment = new FragmentCarte();
                break;
            case R.id.nav_promo:

                sendNotification("hello","My message","www.facebook.com");

               // push(new FragmentPromotions());
                /*Intent activityPromotions = new Intent(this,ActivityPromotions.class);
                startActivity(activityPromotions);
                fragment = new FragmentPromotions();*/
                break;

            case R.id.nav_partenaries:

                push(new FragmentPartenaires());

               /* Intent activityPartenaries = new Intent(this,ActivityPartenaries.class);
                startActivity(activityPartenaries);*/
                break;

            case R.id.nav_magasin:

                push(new FragmentMagasin());

               /* Intent activityMagsin = new Intent(this,ActivityMagsin.class);
                startActivity(activityMagsin);*/
                break;
            case R.id.nav_conta:

                push(new FragmentContactUs());

               /* Intent activityContactUs = new Intent(this,ActivityContactUs.class);
                startActivity(activityContactUs);*/
                break;
            case R.id.nav_ins:
                Intent activityIntroduction = new Intent(this,ActivityIntroduction.class);
                startActivity(activityIntroduction);
                break;
            case R.id.nav_prop:

                push(new FragmentApropos());

                /*Intent activityApropos = new Intent(this,ActivityApropos.class);
                startActivity(activityApropos);*/
                break;
            case R.id.nav_men:

                push(new FragmentConditionUtilisation());

                /*Intent activityMentionLegales = new Intent(this,ActivityConditionUtilisation.class);
                startActivity(activityMentionLegales);*/
                break;

            case R.id.nav_not:
                push(new FragmentNotification());

                break;

                default:
                    push(new FragmentCarte());
                   // fragment = new FragmentCarte();
                    break;

        }
        if(drawer!=null && drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawers();
        }

      /*  if (fragment != null) {
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment);
            ft.commit();
        }*/
    }

    private void sendNotification(String message, String title, String url) {
        int NOTIFICATION_ID= 1;
        int importance = NotificationManager.IMPORTANCE_HIGH;
//        Intent intent = new Intent(this, ActivitySplash.class);
//        intent.putExtra("isFromNotification", true);
        String channelId = "Club_Jumbo";
        String channelName = "Common Notification";

        RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.notification_large);


        Intent radio=new Intent(this, ActivitySplash.class);
        radio.putExtra("url", url);//if necessary




        PendingIntent pRadio = PendingIntent.getActivity(this, 0, radio, 0);

        notificationLayout.setTextViewText(R.id.textTitle,title);
        notificationLayout.setTextViewText(R.id.textDescription,message);
        notificationLayout.setOnClickPendingIntent(R.id.relativeContent,pRadio);

        Intent switchIntent = new Intent(this, ActivityNavigation.switchButtonListener.class);
        switchIntent.putExtra("notificationId",NOTIFICATION_ID);

        PendingIntent pendingSwitchIntent = PendingIntent.getBroadcast(this, 1, switchIntent, PendingIntent.FLAG_ONE_SHOT);
        notificationLayout.setOnClickPendingIntent(R.id.textToast, pendingSwitchIntent);

        NotificationCompat.Action replyAction = new NotificationCompat.Action.Builder(
                android.R.drawable.ic_menu_close_clear_cancel, "View", pendingSwitchIntent)
                .build();

        NotificationCompat.Action dismissAction = new NotificationCompat.Action.Builder(
                android.R.drawable.ic_menu_close_clear_cancel, "DISMISS", pendingSwitchIntent)
                .build();


        Notification customNotification = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.club_notify_small_icon)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(notificationLayout)
                .setContentIntent(pRadio)
                .addAction(replyAction)
                .addAction(dismissAction)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .build();

        NotificationManager mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            mNotificationManager.createNotificationChannel(mChannel);
        }
        mNotificationManager.notify(NOTIFICATION_ID ,customNotification);


    }

    public static class switchButtonListener extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            int notificationId = intent.getIntExtra("notificationId", 0);
            if (notificationId!=0){
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(notificationId);
            }


        }
    }
    
    public void savefcm() {

        APIinterface apiService = APIClient.getClient().create(APIinterface.class);
            Call<FirebaseModel> call = apiService.savefcm(android_id,regId,"android");
            call.enqueue(new Callback<FirebaseModel>() {
                @Override
                public void onResponse(Call<FirebaseModel> call, Response<FirebaseModel> response) {
                    try {
                        if(response.body().getResult()!= null){
                            model = response.body().getResult();
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(Call<FirebaseModel> call, Throwable t) {
                    //failurecase
                    System.out.println("Error"+ t.getMessage());
                }
            });

    }

    public void popBackStackImmediate() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.executePendingTransactions();
    }

    public void popAllBackstack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backCount = fragmentManager.getBackStackEntryCount();
        if (backCount > 0) {
            fragmentManager.popBackStackImmediate(fragmentManager.getBackStackEntryAt(0).getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }
}
