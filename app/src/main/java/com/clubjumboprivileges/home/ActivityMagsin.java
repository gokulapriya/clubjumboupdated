package com.clubjumboprivileges.home;


import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityMagsin extends AppCompatActivity {
    ProgressDialog progressDialog;
    ImageView backBtn;
    WebView webViewMagasin;
    TextView textHeader;
    RelativeLayout relativeNointernet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magsin);

        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.parseColor("#000000"));
            }
            getSupportActionBar().hide();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        progressDialog=new ProgressDialog(this);
        webViewMagasin = findViewById(R.id.wvv2);
        backBtn=findViewById(R.id.backBtn);
        relativeNointernet = findViewById(R.id.relativeNointernet);
        textHeader = findViewById(R.id.textHeader);
        textHeader.setText(R.string.nos_magasin);

        if(isInternetOn()) {

            relativeNointernet.setVisibility(View.GONE);
            webViewMagasin.setVisibility(View.VISIBLE);

            progressDialog.setMessage(getString(R.string.loading));
            webViewMagasin.loadUrl("https://jumbo.mu/magasinminipage");
            WebSettings webSettings = webViewMagasin.getSettings();
            webSettings.setJavaScriptEnabled(true);
            progressDialog.show();
        }
        else
        {
            relativeNointernet.setVisibility(View.VISIBLE);
            webViewMagasin.setVisibility(View.GONE);

            Toast.makeText(this,  R.string.no_internet, Toast.LENGTH_SHORT).show();
        }

        webViewMagasin.setWebViewClient(new WebviewMagasin());
        webViewMagasin.setWebChromeClient(new WebChromeClient());

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webViewMagasin.canGoBack()){
                    webViewMagasin.goBack();
                }
                else{
                    onBackPressed();
                }
            }
        });
    }
    public class WebviewMagasin extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

    @Override
    public void onBackPressed() {
        if (webViewMagasin.canGoBack()){
            webViewMagasin.goBack();
        }
        else{
            super.onBackPressed();
        }
    }

    public final boolean isInternetOn() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm =(ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
