package com.clubjumboprivileges.home;


import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentConditionUtilisation extends Fragment {

    ProgressDialog progressDialog;
    TextView textView;
    /*TextView textHeader;
    ImageView backBtn;*/
    WebView webviewMention;
    RelativeLayout relativeNointernet;




    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_condition_utilisation, container, false);

        progressDialog=new ProgressDialog(getActivity());
        textView=(TextView) view.findViewById(R.id.amenss);
        relativeNointernet = view.findViewById(R.id.relativeNointernet);
        webviewMention = view.findViewById(R.id.webviewMention);
       /* backBtn=(ImageView) view.findViewById(R.id.backBtn);
        textHeader = view.findViewById(R.id.textHeader);
        textHeader.setText(R.string.conditions_utilisation);*/
        ((ActivityNavigation)getActivity()).imgHeader.setVisibility(View.GONE);
        ((ActivityNavigation)getActivity()).textHeader.setVisibility(View.VISIBLE);
        ((ActivityNavigation)getActivity()).textHeader.setText(R.string.conditions_utilisation);
        ((ActivityNavigation)getActivity()).notification.setVisibility(View.VISIBLE);
        ((ActivityNavigation)getActivity()).fragmentPosition=1;


        if(isInternetOn()) {

            relativeNointernet.setVisibility(View.GONE);
            webviewMention.setVisibility(View.VISIBLE);

            progressDialog.setMessage(getString(R.string.loading));
            webviewMention.loadUrl("https://jumbo.mu/conditions-dutilisation-minipage");
            WebSettings webSettings = webviewMention.getSettings();
            webSettings.setJavaScriptEnabled(true);
            progressDialog.show();
        }
        else
        {
            relativeNointernet.setVisibility(View.VISIBLE);
            webviewMention.setVisibility(View.GONE);

            Toast.makeText(getActivity(),  R.string.no_internet, Toast.LENGTH_SHORT).show();
        }

        webviewMention.setWebViewClient(new WebviewMention());
        webviewMention.setWebChromeClient(new WebChromeClient());

        return view;
    }

    public class WebviewMention extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

    public final boolean isInternetOn() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm =(ConnectivityManager)getActivity().getSystemService(getActivity().getBaseContext().CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}
