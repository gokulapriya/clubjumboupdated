package com.clubjumboprivileges.home;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.clubjumboprivileges.database.DatabaseHandler;
import com.clubjumboprivileges.database.Databasevalues;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import java.util.EnumMap;
import java.util.Map;
public class FragmentCarte extends android.support.v4.app.Fragment {

    BroadcastReceiver receiver;
    TextView username;
    ImageView barcode;
    String firstName ="";
    RelativeLayout relativeMagasin;
    RelativeLayout relativeContact;
    TextView barcodenumber;
    SharedPreferences pref;
    RelativeLayout relativePartenaries;
    SharedPreferences.Editor edit;
    DatabaseHandler dbh;
    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    ImageView resetbarcode;
    int height = 600;
    int width = 300;
    RelativeLayout cartelayout;
    RelativeLayout scandetails;
    ImageView delete;
    ImageView cart;
    ImageView pro;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.home_layout, container, false);

        cart =view.findViewById(R.id.cart);
        pro =view.findViewById(R.id.imgpromo);
        relativeMagasin = view.findViewById(R.id.relativeMagasin);
        relativePartenaries = view.findViewById(R.id.relativePartenaries);
        relativeContact = view.findViewById(R.id.relativeContact);
        barcode=view.findViewById(R.id.scanimage);
        username =view.findViewById(R.id.username2);
        barcodenumber=view.findViewById(R.id.barcodeidnumber);
        cartelayout=view.findViewById(R.id.cartereplace2);
        delete=view.findViewById(R.id.delete);
        scandetails=view.findViewById(R.id.scanimagelayout);
        ((ActivityNavigation)getActivity()).imgHeader.setVisibility(View.VISIBLE);
        ((ActivityNavigation)getActivity()).textHeader.setVisibility(View.GONE);
        ((ActivityNavigation)getActivity()).notification.setVisibility(View.VISIBLE);

        initializeListener();



        pref = getActivity().getSharedPreferences("LOYALTY", Context.MODE_PRIVATE);
        edit = pref.edit();
       int  value=pref.getInt("check",0);

       if(value!=0)
       {

           showdetails();
       }
       else
       {
           cartelayout.setVisibility(View.VISIBLE);
           scandetails.setVisibility(View.GONE);
       }
       receiver=new BroadcastReceiver() {
           @Override
           public void onReceive(Context context, Intent intent) {

               int  value=pref.getInt("check",0);

               if(value!=0)
               {

                   showdetails();
               }
               else
               {
                   cartelayout.setVisibility(View.VISIBLE);
                   scandetails.setVisibility(View.GONE);
               }
           }
       };
        IntentFilter filter=new IntentFilter("scan");
        getActivity().registerReceiver(receiver,filter);

        return view;

    }

    private void initializeListener() {

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatedetails();

            }
        });


        relativeContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.support.v4.app.Fragment fragment = null;
                fragment = new FragmentContactUs();
                android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, fragment).commit();

                /*Intent intent4 = new Intent(getActivity(),ActivityContactUs.class);
                startActivity(intent4);*/
            }
        });

        relativeMagasin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.support.v4.app.Fragment fragment = null;
                fragment = new FragmentMagasin();
                android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, fragment).commit();

                /*Intent intent3 = new Intent(getActivity(),ActivityMagsin.class);
                startActivity(intent3);*/
            }
        });

        scandetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ActivityBarcode.class);
                startActivity(intent);

            }
        });
        relativePartenaries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                android.support.v4.app.Fragment fragment = null;
                fragment = new FragmentPartenaires();
                android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, fragment).commit();

                /*Intent intent = new Intent(getActivity(),ActivityPartenaries.class);
                startActivity(intent);*/
            }
        });
        pro.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {


                android.support.v4.app.Fragment fragment = null;
                fragment = new FragmentPromotions();
                android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, fragment).commit();

              //  ((ActivityNavigation)getActivity()).push(new FragmentPromotions(), "dfddf");


                /*Intent intent = new Intent(getActivity(),ActivityPromotions.class);
                startActivity(intent);*/

            }
        });

    }

    void showdetails()
    {
        cartelayout.setVisibility(View.GONE);
        scandetails.setVisibility(View.VISIBLE);
        try {
            loadData();
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
    }

    public void updatedetails(){
        Intent intent = new Intent(getActivity(),ActivityMaCarte.class);
        startActivity(intent);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Club_Jumbo");
    }

    private void loadData() throws WriterException {


        dbh = new DatabaseHandler(getActivity());
        dbh.opendb();
        Cursor cursor = dbh.getallrecords();
        if (cursor.moveToFirst()) {

            while (!cursor.isAfterLast()) {
                int userId = cursor.getInt(cursor
                        .getColumnIndex("User_id"));
                firstName = cursor.getString(cursor
                        .getColumnIndex(Databasevalues.USER_NAME));
                String barcodedata = cursor.getString(cursor
                        .getColumnIndex(Databasevalues.BAR_CODE_DATA));
                String format = cursor.getString(cursor
                        .getColumnIndex(Databasevalues.FORMAT));

                edit.putInt("Userid", userId);
                edit.commit();
           username.setText(firstName);
           barcodenumber.setText(barcodedata);

                Bitmap bmp = null;

                if(!format.equals(""))
                {
                    if(format.equalsIgnoreCase("AZTEC"))
                    {
                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.AZTEC, height, width);

                    }else if(format.equalsIgnoreCase("CODABAR"))
                    {
                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.CODABAR, height, width);


                    }else if(format.equalsIgnoreCase("CODE_39"))
                    {
                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.CODE_39, height, width);


                    }else if(format.equalsIgnoreCase("CODE_93"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.CODE_93, height, width);

                    }else if(format.equalsIgnoreCase("CODE_128"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.CODE_128, height, width);

                    }else if(format.equalsIgnoreCase("DATA_MATRIX"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.DATA_MATRIX, height, width);
                        Drawable d = new BitmapDrawable(getResources(), bmp);
                        barcode.setImageDrawable(d);
                    }else if(format.equalsIgnoreCase("EAN_8"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.EAN_8, height, width);

                    }else if(format.equalsIgnoreCase("EAN_13"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.EAN_13, height, width);


                    }else if(format.equalsIgnoreCase("ITF"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.ITF, height, width);

                    }else if(format.equalsIgnoreCase("MAXICODE"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.MAXICODE, height, width);

                    }
                    else if(format.equalsIgnoreCase("PDF_417"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.PDF_417, height, width);

                    }else if(format.equalsIgnoreCase("QR_CODE"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.QR_CODE, height, width);

                    }else if(format.equalsIgnoreCase("RSS_14"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.RSS_14, height, width);

                    }else if(format.equalsIgnoreCase("RSS_EXPANDED"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.RSS_EXPANDED, height, width);

                    }else if(format.equalsIgnoreCase("UPC_A"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.UPC_A, height, width);

                    }else if(format.equalsIgnoreCase("UPC_E"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.UPC_E, height, width);

                    }else if(format.equalsIgnoreCase("UPC_EAN_EXTENSION"))
                    {

                        bmp = encodeAsBitmap(barcodedata, BarcodeFormat.UPC_EAN_EXTENSION, height, width);
                    }
                    Drawable d = new BitmapDrawable(getResources(), bmp);
                    int sdk = android.os.Build.VERSION.SDK_INT;
                    if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        barcode.setImageDrawable(d);
                    } else {
                        barcode.setImageDrawable(d);
                    }

                }
                cursor.moveToNext();
            }
        }
        dbh.closeconnection();

    }

    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int imgWidth, int imgHeight) throws WriterException {
        if (contents == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contents);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contents, format, imgWidth, imgHeight, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int resultWidth = result.getWidth();
        int resultHeight = result.getHeight();
        int[] pixels = new int[resultWidth * resultHeight];
        for (int y = 0; y < resultHeight; y++) {

            int offset = y * resultWidth;
            for (int x = 0; x < resultWidth; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(resultWidth, resultHeight,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, resultWidth, 0, 0, resultWidth, resultHeight);
        return bitmap;
    }
    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }

}