package com.clubjumboprivileges.home;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.clubjumboprivileges.database.DatabaseHandler;
import com.clubjumboprivileges.database.Databasevalues;
import com.clubjumboprivileges.utils.Alertdialog;
import com.clubjumboprivileges.utils.CheckingNetworkAvailable;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;



public class FragmentMycardview extends Fragment implements OnClickListener {
	ListView lv;
	ArrayList<HashMap<String, String>> barcodelist = new ArrayList<HashMap<String,String>>();
	DatabaseHandler dbh;	
	private static final int WHITE = 0xFFFFFFFF;
	private static final int BLACK = 0xFF000000;	
	SharedPreferences pref;
	Editor edit;
	String first_name="";
	TextView  Username;
	ImageView barcode;
	TextView barcodenumber;
	ImageView editbutton;
	EditText useredit;
	LinearLayout borderbottom;
	TextView editmessage;
	int i = 0;
	boolean barcodeclickable = false;
	Button resetbarcode;
	Alertdialog dialog;
	CheckingNetworkAvailable font;
	int height = 600;
	int width = 300;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_mycardview, container, false);
		
		Init(rootView);
		
		font = new CheckingNetworkAvailable(getActivity());
		font.setTextviewtypeface(4, Username);
		font.setTextviewtypeface(4, resetbarcode);
		Display mDisplay = getActivity().getWindowManager().getDefaultDisplay();

		
		if(pref.getBoolean("Editstatus", true))
		{
			editbutton.setVisibility(View.VISIBLE);
			
		}else{
			editbutton.setVisibility(View.GONE);
			Intent in=new Intent("settype");
			in.putExtra("name",2);
			getActivity().sendBroadcast(in);
		}

		editbutton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(i==1)
				{
					Username.setVisibility(View.VISIBLE);
					Username.setText(useredit.getText().toString());
					useredit.setVisibility(View.GONE);
					i=0;
					borderbottom.setVisibility(View.VISIBLE);
					barcodeclickable = false;
					editmessage.setVisibility(View.GONE);
					editbutton.setBackgroundResource(R.drawable.editimage);
					dbh.opendb();
					dbh.updatename(useredit.getText().toString(),"",pref.getInt("Userid", 0));
					dbh.closeconnection();
				}else if(i==0){
					Username.setVisibility(View.GONE);
					useredit.setText(Username.getText().toString());
					useredit.setVisibility(View.VISIBLE);
					editbutton.setBackgroundResource(R.drawable.saveimage);
					borderbottom.setVisibility(View.GONE);
					i=1;
					barcodeclickable = true;
					editmessage.setVisibility(View.VISIBLE);
				}
			}
		});
		barcode.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(barcodeclickable)
				{
					Fragment fragment = null;
					fragment = new FragmentJeSaisisMacarte();
					FragmentManager fragmentManager = getActivity().getFragmentManager();
					fragmentManager.beginTransaction()
					.replace(R.id.containerLayout, fragment).commit();
					
				}
			}
		});
		
		
			
		try {
			Loaddatas();
		} catch (WriterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		pref = getActivity().getSharedPreferences("LOYALTY", Context.MODE_PRIVATE);	
		edit = pref.edit();		


		return rootView;
	}

	private void Init(View view) {
		// TODO Auto-generated method stub
		pref = getActivity().getSharedPreferences("LOYALTY", Context.MODE_PRIVATE);
		edit = pref.edit();		
		editbutton = (ImageView)view.findViewById(R.id.editbutton);
		editmessage = (TextView)view.findViewById(R.id.editmessage);
		useredit = (EditText)view.findViewById(R.id.useredit);
		borderbottom = (LinearLayout)view.findViewById(R.id.borderbottom);
		dbh = new DatabaseHandler(getActivity());
		Username = (TextView)view.findViewById(R.id.username);
		barcode = (ImageView)view.findViewById(R.id.barcode);
		barcodenumber = (TextView)view.findViewById(R.id.barcodenumber);
		editbutton.setBackgroundResource(R.drawable.editimage);
		resetbarcode = (Button)view.findViewById(R.id.resetbarcode);
		resetbarcode.setOnClickListener(this);
	}


	private void Loaddatas() throws WriterException {
		dbh.opendb();
		Cursor cursor = dbh.getallrecords();	
		if (cursor.moveToFirst()) {

			while (cursor.isAfterLast() == false) {	            	
				int User_id = cursor.getInt(cursor
						.getColumnIndex("User_id"));	                
				first_name = cursor.getString(cursor
						.getColumnIndex(Databasevalues.USER_NAME));				
				String barcodedata = cursor.getString(cursor
						.getColumnIndex(Databasevalues.BAR_CODE_DATA));
				String dateandtime = cursor.getString(cursor
						.getColumnIndex(Databasevalues.DATEANDTIME));
				String format = cursor.getString(cursor
						.getColumnIndex(Databasevalues.FORMAT));	
				

				
				edit.putInt("Userid", User_id);
				edit.commit();
				Username.setText(first_name);				
				barcodenumber.setText(barcodedata);
				
				Bitmap bmp = null;
				
				if(!format.equals(""))
				{
					if(format.equalsIgnoreCase("AZTEC"))
					{						
						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.AZTEC, height, width);
						
					}else if(format.equalsIgnoreCase("CODABAR"))
					{						
						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.CODABAR, height, width);
					
						
					}else if(format.equalsIgnoreCase("CODE_39"))
					{						
						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.CODE_39, height, width);
						
						
					}else if(format.equalsIgnoreCase("CODE_93"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.CODE_93, height, width);
						
					}else if(format.equalsIgnoreCase("CODE_128"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.CODE_128, height, width);
						
					}else if(format.equalsIgnoreCase("DATA_MATRIX"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.DATA_MATRIX, height, width);
						Drawable d = new BitmapDrawable(getResources(), bmp);
						barcode.setBackgroundDrawable(d);
					}else if(format.equalsIgnoreCase("EAN_8"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.EAN_8, height, width);
						
					}else if(format.equalsIgnoreCase("EAN_13"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.EAN_13, height, width);
						

					}else if(format.equalsIgnoreCase("ITF"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.ITF, height, width);
						
					}else if(format.equalsIgnoreCase("MAXICODE"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.MAXICODE, height, width);
						
					}
					else if(format.equalsIgnoreCase("PDF_417"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.PDF_417, height, width);
						
					}else if(format.equalsIgnoreCase("QR_CODE"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.QR_CODE, height, width);
						
					}else if(format.equalsIgnoreCase("RSS_14"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.RSS_14, height, width);
						
					}else if(format.equalsIgnoreCase("RSS_EXPANDED"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.RSS_EXPANDED, height, width);
						
					}else if(format.equalsIgnoreCase("UPC_A"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.UPC_A, height, width);
						
					}else if(format.equalsIgnoreCase("UPC_E"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.UPC_E, height, width);
						
					}else if(format.equalsIgnoreCase("UPC_EAN_EXTENSION"))
					{

						bmp = encodeAsBitmap(barcodedata, BarcodeFormat.UPC_EAN_EXTENSION, height, width);						
					}
					Drawable d = new BitmapDrawable(getResources(), bmp);
					int sdk = android.os.Build.VERSION.SDK_INT;
					if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {					   
					    barcode.setBackgroundDrawable(d);
					} else {					    
					    barcode.setBackground(d);
					}

				}

				cursor.moveToNext();
			}

		}
		dbh.closeconnection();

	}


	Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
		String contentsToEncode = contents;
		if (contentsToEncode == null) {
			return null;
		}
		Map<EncodeHintType, Object> hints = null;
		String encoding = guessAppropriateEncoding(contentsToEncode);
		if (encoding != null) {
			hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
			hints.put(EncodeHintType.CHARACTER_SET, encoding);
		}
		MultiFormatWriter writer = new MultiFormatWriter();
		BitMatrix result;
		try {
			result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
		} catch (IllegalArgumentException iae) {
			// Unsupported format
			return null;
		}
		int width = result.getWidth();
		int height = result.getHeight();
		int[] pixels = new int[width * height];
		for (int y = 0; y < height; y++) {
			int offset = y * width;
			for (int x = 0; x < width; x++) {
				pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
			}
		}

		Bitmap bitmap = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		return bitmap;
	}
	private static String guessAppropriateEncoding(CharSequence contents) {
		// Very crude at the moment
		for (int i = 0; i < contents.length(); i++) {
			if (contents.charAt(i) > 0xFF) {
				return "UTF-8";
			}
		}
		return null;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.resetbarcode:
			/*if(HomeActivity.mDrawerLayout!=null)
				dialog = new Alertdialog(getActivity(), ActivityScan.mDrawerLayout);*/
			break;

		default:
			break;
		}
	}



}