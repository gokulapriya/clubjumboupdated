package com.clubjumboprivileges.home;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityConditionUtilisation extends AppCompatActivity {

    ProgressDialog progressDialog;
    TextView textView;
    TextView textHeader;
    ImageView backBtn;
    WebView webviewMention;
    RelativeLayout relativeNointernet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_condition_utilisation);

        progressDialog=new ProgressDialog(this);
        textHeader = findViewById(R.id.textHeader);
        textView=(TextView)findViewById(R.id.amenss);
        backBtn=(ImageView)findViewById(R.id.backBtn);
        relativeNointernet = findViewById(R.id.relativeNointernet);
        webviewMention = findViewById(R.id.webviewMention);
        textHeader.setText(R.string.conditions_utilisation);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#000000"));
        }

        if(isInternetOn()) {

            relativeNointernet.setVisibility(View.GONE);
            webviewMention.setVisibility(View.VISIBLE);

            progressDialog.setMessage(getString(R.string.loading));
            webviewMention.loadUrl("https://jumbo.mu/conditions-dutilisation-minipage");
            WebSettings webSettings = webviewMention.getSettings();
            webSettings.setJavaScriptEnabled(true);
            progressDialog.show();
        }
        else
        {
            relativeNointernet.setVisibility(View.VISIBLE);
            webviewMention.setVisibility(View.GONE);

            Toast.makeText(this,  R.string.no_internet, Toast.LENGTH_SHORT).show();
        }

        webviewMention.setWebViewClient(new WebviewMention());
        webviewMention.setWebChromeClient(new WebChromeClient());



        String aboutt ="<h3>"+"<font color='#345a8a'>"+"1/ Conditions d’adhésion • "+"</font>"+"</h3>"+
                "<h4>"+"<font color='#4f81bd'>"+"   1. Comment se la procurer ?"+"</font>"+"</h4>"+
                "<font color='#000000'>"+"La Carte MY JUMBO LE CLUB est gratuite et le bulletin d’adhésion est disponible dans les magasins Jumbo et sur le site internet www.jumbo.mu. •"+"</font>"+
                "<h4>"+"<font color='#4f81bd'>"+"Conditions d’attribution"+"</font>"+"</h4>"+
                "<font color='#000000'>"+"<font color='#000000'>"+"A) Les personnes physiques, âgées de plus de 18 ans, peuvent se faire remettre une Carte MY JUMBO LE CLUB et en faire valoir les avantages dans les conditions visées dans le présent document. "+"</font>"+
                "<br>"+"B) Les entités collectives telles que les associations ou les entreprises (autre que professionnels de la distribution, grossistes etc) peuvent également se faire attribuer une Carte MY JUMBO LE CLUB, sous réserve de la remise, lors de la demande de celle-ci, par la personne chargée de la demande d’attribution de la Carte, d’une attestation établie sur du papier à l’entête de l’entité collective concernée, émanant du représentant légal de celle-ci, précisant l’identité de la personne physique concernée qui sera mentionnée sur la Carte MY JUMBO LE CLUB attribuée pour le compte de l’entité collective concernée, ainsi que son habilitation à effectuer les démarches nécessaires à l’obtention de la Carte MY JUMBO LE CLUB. Cette attestation devra en outre préciser que l’entité collective concernée garantit que les magasins Jumbo ne pourront en aucun cas être mis en cause, collectivement ou individuellement, du fait du non-respect des conditions d’utilisation de la Carte MY JUMBO LE CLUB par la personne désignée sur la Carte, ou de l’utilisation des avantages qu’elle offre à titre personnel et non au bénéfice de l’entité collective qui l’a mandatée. "+
                "<br>"+"C) Lors de l’attribution de la Carte MY JUMBO LE CLUB, le futur titulaire de la Carte est invité à opter pour un magasin principal, de référence, ayant fonction de point de contact du titulaire. \n" +
                "Le titulaire de la Carte MY JUMBO LE CLUB reconnaît que le choix d’un magasin principal subordonne la remise de la Carte, et valide son accord express de recevoir des informations. " +
                "Le titulaire de la Carte MY JUMBO LE CLUB peut à tout moment modifier son choix de magasin principal en se rendant à l’accueil du magasin qu’il souhaite désormais désigner comme magasin principal. "+"</font>"+
                "<h3>"+"<br>"+"<font color='#345a8a'>"+"2/ Conditions d’utilisation"+"</font>"+"</h3>"+
                "<font color='#000000'>"+"A) La Carte MY JUMBO LE CLUB comporte un numéro d’identification personnel (“Compte”), qui ne peut être cédé.  "+
                "<br>"+"B) La Carte MY JUMBO LE CLUB est utilisable dans tous les magasins Jumbo participant au programme de fidélité (liste des magasins disponible sur www.jumbo.mu).  "+
                "<br>"+"C) En cas de non-utilisation de la Carte MY JUMBO LE CLUB par son titulaire pendant une période continue de 6 mois, le magasin Jumbo auquel est rattachée la Carte se réserve le droit de supprimer toute information relative à cette carte, dont les avantages cumulés, sans information préalable du titulaire.  "+
                "<br>"+"D) Toute utilisation frauduleuse, entrainera l’exclusion du programme Carte MY JUMBO LE CLUB ainsi que la perte des avantages acquis frauduleusement.  "+
                "<br>"+"E) La carte Privilèges est à usage strictement personnel. Elle ne peut être ni cédée ni prêtée. "+"</font>"+
                "<h3>"+"<br>"+"<font color='#345a8a'>"+"3/ Avantages liés à la présentation de la Carte MY JUMBO LE CLUB lors du passage en caisse"+"</font'>"+"</h3>"+
                "<font color='#000000'>"+" A) La Carte MY JUMBO LE CLUB n’est pas une carte de paiement. "+
                "<br>"+"B) La Carte MY JUMBO LE CLUB permet à son titulaire, sur présentation en caisse de sa Carte lors des achats, de cumuler le cas échéant CASH. La Carte MY JUMBO LE CLUB doit impérativement être présentée des le début du passage en caisse.Les CASH sont valables dans les magasins Jumbo participant au programme de fidélité (liste des magasins disponible sur le site www.jumbo.mu) Les CASH "+
                "<br>"+"  • peuvent être utilisés pour payer tous les achats. "+
                "<br>"+"  • sont valables dès l’achat suivant. "+
                "<br>"+"C) En cas de retour d’achat d’un produit ayant généré l’octroi d’un CASH et sur présentation du ticket de caisse dans les magasins Jumbo ou Spar, le magasin concerné débitera du Compte rattaché à la Carte MY JUMBO LE CLUB, le montant du CASH correspondant au montant du CASH attaché au produit retourné et remboursé par le magasin. \n" +
                "<br>"+"Si le titulaire de la Carte MY JUMBO LE CLUB a utilisé tout ou partie du CASH obtenu lors de l’achat du produit retourné, entre le moment de l’acquisition et le remboursement du produit l’ayant généré, le titulaire de la Carte MY JUMBO LE CLUB devra rembourser au magasin concerné la différence dans l’hypothèse où le solde de la Carte MY JUMBO LE CLUB serait insuffisant pour couvrir le remboursement du montant du CASH généré par l’achat initial du produit retourné. "+"</font>"+
                "<h3>"+"<br>"+"<font color='#345a8a'>"+"4/ Perte, vol ou détérioration de la Carte MY JUMBO LE CLUB "+"</font>"+"</h3>"+
                "<font color='#000000'>"+"En cas de perte, de vol ou de détérioration de la Carte MY JUMBO LE CLUB, le titulaire doit se rendre dans les meilleurs délais à l’accueil du magasin qu’il avait désigné comme magasin principal et présenter une pièce d’identité en cours de validité. Une nouvelle Carte pourra lui être remise en tenant compte des délais de fabrication, et le total des CASH cumulés sur l’ancienne Carte et non utilisés à la date de la demande de remise d’une nouvelle Carte MY JUMBO LE CLUB lui sera transféré sur la nouvelle."+
                "<br>"+"Entre le moment où la Carte a été perdue ou volée et le moment où la déclaration de perte ou de vol est effectuée à l’accueil du magasin principal, le titulaire de la Carte MY JUMBO LE CLUB reconnaît que les magasins Jumbo, individuellement ou collectivement, sont dégagés de toute responsabilité."+"</font>"+
                "<h3>"+"<br>"+"<font color='#345a8a'>"+"5/ Évolutions"+"</font>"+"<h3>"+
                "<h4>"+"<font color='#4f81bd'>"+"• Arrêt ou modifications du programme de fidélité"+"</font>"+"</h4>"+
                "<font color='#000000'>"+"L’enseigne Jumbo se réserve le droit de modifier ou de supprimer à tout moment, sous réserve de faire une communication par voie d’affichage a l accueil de nos magasins, en respectant un préavis d’un mois, le programme de fidélité Carte MY JUMBO LE CLUB (conditions d’adhésion, avantages liés à la Carte MY JUMBO LE CLUB, ...) et/ ou les modalités de l’utilisation de la Carte MY JUMBO LE CLUB. Aucune modification, y compris la cessation du programme de fidélité, n’ouvrira droit à une quelconque indemnité pour le titulaire de la Carte."+"</font>"+
                "<h4>"+"<font color='#4f81bd'>"+"• Évolution ou changement des présentes Conditions Générales "+"</font>"+"</h4>"+
                "<font color='#000000'>"+"Les présentes Conditions Générales sont susceptibles d’évolutions et de modifications à tout moment et sans préavis de la part des enseignes Jumbo. Pour prendre connaissance des éventuels changements qui auraient été apportés aux présentes conditions, il suffit au titulaire de la Carte MY JUMBO LE CLUB de se rendre sur le site www.jumbo.mu ou, d’en demander un exemplaire à jour auprès de son magasin principal. "+"</font>"+
                "<h4>"+"<font color='#4f81bd'>"+"• Manquement au règlement ou utilisation abusive de la Carte MY JUMBO LE CLUB "+"</font>"+"</h4>"+
                "<font color='#000000'>"+"Les enseignes Jumbo, collectivement ou individuellement, déclinent toute responsabilité en cas d’utilisation non conforme de la Carte MY JUMBO LE CLUB ou CASH acquis. Ils se réservent le droit d’engager toute action qu’ils jugeraient utile en cas d’utilisation frauduleuse ou abusive de la Carte MY JUMBO LE CLUB. Tout manquement aux présentes conditions générales, toute falsification d’informations transmises, entraînerait de plein droit la radiation du titulaire de la Carte MY JUMBO LE CLUB et l’annulation de ses CASH et avantages obtenus à la date de la radiation. "+"</font>"+
                "<h3>"+"<br>"+"<font color='#345a8a'>"+"6/ Utilisation des fichiers de collecte de données personnelles  "+"</font>"+"</h3>"+
                "<font color='#000000'>"+"A) Les données nominatives recueillies dans le bulletin d’adhésion font l’objet de différents traitements informatisés. Les données obligatoires pour souscrire à l’adhésion de la carte MY JUMBO LE CLUB sont mentionnées par une astérisque sur le bulletin d’adhésion. Ces fichiers de données personnelles ont pour finalité d’assurer la délivrance, la gestion de l’ensemble des opérations attachées à la carte, la réalisation de statistiques et analyses commerciales et en cas d’acceptation, la prospection commerciale. Les fichiers sont destinés à l’ensemble des enseignes Jumbo et prestataires en charge de la gestion de la carte. Votre magasin procède également à la collecte des données relatives aux achats afin de gérer le suivi après-vente à la demande du porteur ou du magasin. Cette collecte est obligatoire. "+
                "<br>"+"B) Conformément au respect du principe de la clause OPT IN, le client est en droit de refuser l’utilisation à des fins de prospection commerciale, de ses données personnelles collectées à l’aide de son bulletin d’adhésion ou saisies électroniquement via le site Internet www.jumbo.mu. En conséquence, en cas d’acceptation expresse de l’utilisation de ses coordonnées par les entités Jumbo et/ou par leurs partenaires ceux-ci pourront lui adresser des messages commerciaux soit par courriel, soit par SMS (via son numéro de GSM). "+"</font>"+
                "<h4>"+"<br>"+"<font color='#4f81bd'>"+"• Droit d’accès, de modification et de retrait des données personnelles"+"</font>"+"</h4>"+
                "<font color='#000000'>"+"Conformément à la loi, le titulaire de la Carte MY JUMBO LE CLUB dispose d’un droit d’accès et de rectification des données le concernant, à l’accueil du magasin principal désigné par le titulaire lors de la souscription de la Carte MY JUMBO LE CLUB ou ultérieurement. Toute demande de retrait des données personnelles implique que le titulaire de la Carte MY JUMBO LE CLUB ne sera plus porteur de la carte MY JUMBO LE CLUB. Le titulaire de la Carte MY JUMBO LE CLUB garantit l’exactitude des informations fournies et sera seul responsable de toutes les indications erronées, incomplètes ou obsolètes, qui engendreraient un préjudice pour lui, dont la perte des avantages liés à la Carte MY JUMBO LE CLUB. ";

      //  textView.setText(Html.fromHtml(aboutt));
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
    }
    public class WebviewMention extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

    public final boolean isInternetOn() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm =(ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
