package com.clubjumboprivileges.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clubjumboprivileges.Model_class.NotificationModel;
import com.clubjumboprivileges.home.ActivityNavigation;
import com.clubjumboprivileges.home.ActivityNotification;
import com.clubjumboprivileges.home.ExpandableTextView;
import com.clubjumboprivileges.home.FragmentNotificationWebview;
import com.clubjumboprivileges.home.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;


/**
 * Created by GP on 10/18/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    ArrayList<NotificationModel.resulst> notificationList;
    private Context context;
    int feedId;
    boolean expandable = true;

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;


    public NotificationAdapter(ArrayList<NotificationModel.resulst> notificationArray, Context context) {

        this.notificationList =notificationArray;
        this.context=context;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvMsg,tvDate,tv_description,textViewMore;
        RelativeLayout relative_content;


        public MyViewHolder(View view) {
            super(view);
            tvMsg = view.findViewById(R.id.tv_message);
            tv_description = view.findViewById(R.id.tv_description);
            tvDate = view.findViewById(R.id.tv_date);
            textViewMore = view.findViewById(R.id.textViewMore);
            relative_content = view.findViewById(R.id.relative_content);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final NotificationModel.resulst notificationModel = notificationList.get(position);



        holder.tvMsg.setText(notificationModel.getTitle().replace("null",""));
        holder.tv_description.setText(notificationModel.getDescription().replace("null",""));
        holder.tvDate.setText(covertTimeToText(notificationModel.getCreated_date()));
        holder.tv_description.setMaxLines(2);
        if (holder.tv_description.length() < 80){
            holder.textViewMore.setVisibility(View.GONE);
        }
        else {
            holder.textViewMore.setVisibility(View.VISIBLE);
        }
        //holder.tvDate.setText(notificationModel.getCreated_date().replace(" null",""));

        holder.textViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandable){
                    holder.tv_description.setMaxLines(6);
                    holder.textViewMore.setText("ViewLess");
                    expandable = false;
                }
                else {
                    holder.tv_description.setMaxLines(2);
                    holder.textViewMore.setText("ViewMore");
                    expandable = true;
                }

            }
        });

       /* try {
            SimpleDateFormat dateFormatParse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat printFormat = new SimpleDateFormat("HH:mm:ss");
            String targetDate = notificationModel.getCreated_date();
            Date dateString = dateFormatParse.parse(targetDate);
            String timeago = getTimeAgo(dateString.getTime(),context);
           // String niceDateStr = (String) DateUtils.getRelativeTimeSpanString(dateString.getTime() , Calendar.getInstance().getTimeInMillis(), DateUtils.MINUTE_IN_MILLIS,DateUtils.FORMAT_ABBREV_RELATIVE);
            String date[] = notificationModel.getCreated_date().split(" ");
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = fmt.parse(date[0]);

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd/MM/yy");
            fmtOut.format(date1);
         //   holder.tvDate.setText(timeago);
            //holder.tvDate.setText(covertTimeToText(new Date(Long.parseLong("" + dateString.getTime())), fmtOut.format(date1)));
            //System.out.println("getCreated_at ==> 2 "+covertTimeToText(new Date(Long.parseLong("" + dateString.getTime())), date[0]));
        } catch (ParseException e) {
            e.printStackTrace();
        }*/




        holder.relative_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* Matcher m = Patterns.WEB_URL.matcher(notificationModel.getDescription().replace(" null",""));
                while (m.find()) {
                    url = m.group();
                }*/
                if(!notificationModel.getUrl_link().equals("")){
                    Intent intent = new Intent(context,ActivityNotification.class);
                    intent.putExtra("firebaseurl",notificationModel.getUrl_link());
                    context.startActivity(intent);
                    /*Fragment notificationWebviewFragment = new FragmentNotificationWebview();
                    Bundle bundle = new Bundle();
                    bundle.putString("url", url);
                    notificationWebviewFragment.setArguments(bundle);
                    ((ActivityNavigation) context).push(notificationWebviewFragment);*/
                }else{
                    Toast.makeText(context, "No Link", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    public String covertTimeToText(String dataDate) {

        String convTime = null;

        String prefix = "";
        String suffix = "ago";
        //"2019-07-05 12:09:28"
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date pasTime = dateFormat.parse(dataDate);

            Date nowTime = new Date();

            long dateDiff = nowTime.getTime() - pasTime.getTime();

            long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
            long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
            long hour   = TimeUnit.MILLISECONDS.toHours(dateDiff);
            long day  = TimeUnit.MILLISECONDS.toDays(dateDiff);

            if (second < 60) {
                convTime = second+" Seconds "+suffix;
            } else if (minute < 60) {
                convTime = minute+" Minutes "+suffix;
            } else if (hour < 24) {
                convTime = hour+" Hours "+suffix;
            }
            else if(day == 1){
                convTime = day+" day "+suffix;
            }
            else if (day >= 7) {
                if (day > 30) {
                    convTime = (day / 30)+" months "+suffix;
                } else if (day > 360) {
                    convTime = (day / 360)+"years "+suffix;
                } else {
                    convTime = (day / 7) + " week "+suffix;
                }
            } else if (day < 7) {
                convTime = day+" days "+suffix;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ConvTimeE", e.getMessage());
        }

        return convTime;

    }

    public static String getTimeAgo(long time, Context ctx) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = Calendar.getInstance().getTimeInMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

    public static long getDateInMillis(String srcDate) {
        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "d MMMM yyyy, hh:mm aa");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            return dateInMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

}